RUNIT_SERVICE ?= ""
RUNIT_AUTO_ENABLE ?= "enable"
RUNIT_CONF ?= ""

FILES:${PN} =+ "${sysconfdir}/sv ${sysconfdir}/runit ${localstatedir}/*"

do_install:append() {
	if ${@bb.utils.contains('VIRTUAL-RUNTIME_init_manager', 'runit', "true", "false", d)}; then
		install -d ${D}${sysconfdir}/sv
		install -d ${D}${sysconfdir}/runit
		install -d ${D}${sysconfdir}/runit/runsvdir/default
		install -d ${D}${localstatedir}

		ln -sf /run/runit/runsvdir/current ${D}${localstatedir}/service

		for service in ${RUNIT_SERVICE}; do
			servicename="$(basename "$service" .run)"
			install -d ${D}${sysconfdir}/sv/${servicename}
			install -m 0755 ${WORKDIR}/${servicename}.run ${D}${sysconfdir}/sv/${servicename}/run
			ln -sf /run/runit/supervise.${servicename} ${D}${sysconfdir}/sv/${servicename}/supervise

			if test -f "${WORKDIR}/${RUNIT_CONF}"; then
				install -Dm644 ${WORKDIR}/${RUNIT_CONF} ${D}${sysconfdir}/sv/${servicename}/conf
			fi

			if test -f "${WORKDIR}/${servicename}.finish"; then
				install -m 0755 ${WORKDIR}/${servicename}.finish ${D}${sysconfdir}/sv/${servicename}/finish
			fi

			if [ "${RUNIT_AUTO_ENABLE}" = "enable" ]; then
				ln -sf ${sysconfdir}/sv/${servicename} ${D}${sysconfdir}/runit/runsvdir/default
			fi
		done
	fi
}
