SUMMARY = "AC packagegroups"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"
DESCRIPTION = "A packages."

inherit packagegroup

RDEPENDS:${PN} += " \
	acl \
	attr \
	connman \
	connman-client \
	coreutils \
	dbus \
	e2fsprogs \
	eudev \
	iproute2 \
	gawk \
	go \
	kmod \
	libcap \
	lsof \
	mg \
	mmc-utils \
	openssh \
	procps \
	psmisc \
	python3 \
	rng-tools \
	runit \
	runit-stages \
	sed \
	strace \
	util-linux \
	util-linux-agetty \
	usbutils \
	"
