FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI:append = "file://udevd.run"

RUNIT_SERVICE = "udevd.run"
