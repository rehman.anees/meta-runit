SUMMARY = "runit stage and base services"
LIC_FILES_CHKSUM = "file://COPYING;md5=976cd9d5dfe0f556e3793bbd954dc7f4"
LICENSE = "PD"

SRC_URI = "git://gitlab.com/rehman.anees/runit-stages.git;protocol=https;branch=master"

SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git"

FILES:${PN} += "${sysconfdir}"

EXTRA_OEMAKE = 'DESTDIR="${D}"'

do_install () {
    oe_runmake all
}
