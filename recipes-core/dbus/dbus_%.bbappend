FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI:append = "file://dbus.run"

RUNIT_SERVICE = "dbus.run"
