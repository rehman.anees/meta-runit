SUMMARY = "A UNIX init scheme with service supervision"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/PD;md5=b3597d12946881e13cb3b548d1173851"
LICENSE = "PD"

inherit runit

SRC_URI = "http://smarden.org/runit/runit-2.1.2.tar.gz"

SRC_URI:append:class-target = " file://cross-compile.patch \
                                file://match-void-init.patch \
                                file://fix-error-chkshsgr-command-not-found.patch \
"

SRC_URI:append:class-native = " file://fix-native-warning.patch"

SRC_URI[md5sum] = "6c985fbfe3a34608eb3c53dc719172c4"
SRC_URI[sha256sum] = "6fd0160cb0cf1207de4e66754b6d39750cff14bb0aa66ab49490992c0c47ba18"

S = "${WORKDIR}/admin/${BPN}-${PV}"

DEPENDS += "runit-native"
DEPENDS:class-native = ""

do_compile() {
    ./package/compile
}

do_install() {
    install -d ${D}/sbin
}

do_install:append:class-native() {
    install -d ${D}/${bindir}
    install -m 755 ${S}/compile/chkshsgr ${D}${bindir}
}

do_install:append:class-target() {
    install -m755 ${S}/command/* ${D}/sbin
    ln -sf /sbin/runit-init ${D}/sbin/init
}

BBCLASSEXTEND = "native"
