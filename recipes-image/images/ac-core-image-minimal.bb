DESCRIPTION = "AC minimal image"

LICENSE = "MIT"

PROVIDES += "virtual/image"

IMAGE_FEATURES = ""
IMAGE_LINGUAS = ""
IMAGE_FEATURES:append = " debug-tweaks"

IMAGE_INSTALL += "packagegroups-ac-base"

# Avoid installation of syslog
BAD_RECOMMENDATIONS += "busybox-syslog"

# Avoid static /dev
USE_DEVFS = "1"

inherit core-image
