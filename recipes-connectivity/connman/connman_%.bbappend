FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI += "file://connmand.run"
SRC_URI += "file://main.conf"

RUNIT_SERVICE = "connmand.run"
RUNIT_AUTO_ENABLE = "enable"

FILES:${PN} += "${sysconfdir}/connman"

do_install:append() {
	install -d ${D}${sysconfdir}/connman
	install -m644 ${WORKDIR}/main.conf ${D}${sysconfdir}/connman
}
