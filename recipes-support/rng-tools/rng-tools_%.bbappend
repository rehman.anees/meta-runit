FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

inherit runit

SRC_URI:append = "file://rngd.run"

RUNIT_SERVICE = "rngd.run"
